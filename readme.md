[TOC]

Inheriting Aggregate Initialization with `using T::T`
===
# Definitions
*inherit construction*: Inherit the syntax of construction of the base class.

# Abstract
In generic contexts it can be desirable to inherit the constructors of a base class. Currently, this leads to broken code when the base class is an aggregate, as it is no longer possible to initialize the aggregate base's members.

# Motivation
## Aggregate Initialization
There is currently no suscinct way for class `N` to both:
- non-default construct non-aggregate base class `A`, and
- non-default-initialize aggregate base class `B`.
This is a problem in generic code, as a base class may or may not be aggregate.

```CPP
struct A { A(int a, int b, int c){} };
struct B { int a, b, c; };

template <typename T>
struct N : T { using T::T; };

N<A> a{1, 2, 3};                // current: ok, inherits A::A

N<B> b{1, 2, 3};                // current: error, no constructor to inherit(and `N<B>` is now non-aggregate)
                                // proposed: ok, elides the braces, and aggregate constructs

N<B> c{.a = 1, .b = 2, .c = 3}; // current: error, designated initializers cannot be used with a non-aggregate type 'Bar'
                                // proposed: ok, Foo's a, b, and c are initialized via the designated initalizers.
```

# Proposed Changes
The proposed change applies when `T` is an aggregate and a `public` `using T::T` is present:
1. make brace elision mandatory, and
2. the derived class remains aggregate, and
3. for the purposes of designated initializers aggregate elements of `T` shall become aggregate elements of the derived class.

# Issues
## Multiple Inheritance
When dealing with multiple inheritance, there are a few options:
### Option 1
```CPP
// ok: braces are required to be elided.
W<X, Y> opt1_ok{1, 2, 3, 4, 5, 6};
// error: no matching function for call to 'W<X, Y>::W(<brace-enclosed initializer list>)'
W<X, Y> opt1_error{{1, 2, 3}, {4, 5, 6}};
```
Mandate brace elision for each anything with `using T::T`. It is not clear what should be done in this case when a mix of aggregate and non-aggregate base classes are given.

### Option 2
```CPP
// ok: braces are elided as-if no 'using As::As...' was not present.
W<X, Y> opt2_ok{1, 2, 3, 4, 5, 6};
// ok: both bases are elided as-if no 'using As::As...' was not present.
W<X, Y> opt2_error{{1, 2, 3}, {4, 5, 6}};
```
Ignore `using T::T` for aggregates completely.

### Option 3
```CPP
// error: ambiguous brace elision.
W<X, Y> opt3_ok{1, 2, 3, 4, 5, 6};
// error: ambiguous brace elision.
W<X, Y> opt3_error{{1, 2, 3}, {4, 5, 6}};
```
A program is ill-formed when there are multiple elided (`using T::T`) inherited aggregate bases. For this option, either Option 1 and 2 can be applied to the single base.

### Option 4
A combination of `Option 1` and `Option 3`. For a single base class, brace elision is mandated. If an attempt to *inherit construction* of multiple bases is made, the program is ill-formed.  

This option has the advantage of being consistant with the behaviour for non-aggregate classes:
```CPP
struct NonAg
{
	NonAg(int a, int b, int c){}
};

struct Ag
{
	int a, b, c;
};

struct InheritNonAg : NonAg{};
struct InheritAg : Ag{};

InheritAg ag_example{1, 2, 3}; // ok: existing behaviour, brace elision.
InheritAg ag_construct_base{{1, 2, 3}}; // ok: construction of base.
InheritNonAg non_ag_construct_base{{1, 2, 3}}; // ok: construction of base.

template <typename T>
struct InheritConstruction : T { using T::T; };

InheritConstruction<Ag> inherit_construction_ag{1, 2, 3}; // ok: braces are elided. This is what this proposal addresses.
InheritConstruction<Ag> inherit_no_construction_of_base_ag{{1, 2, 3}}; // error: no constructor from std::initialiser_list<int>.
InheritConstruction<NonAg> inherit_construction_non_ag{1, 2, 3}; // ok: constructor is inherited, called directly.
InheritConstruction<NonAg> inherit_no_construction_of_base_non_ag{{1, 2, 3}}; // error: no constructor from std::initialiser_list<int>.
```

When considering multiple bases:
```CPP
struct Glar { Glar(int a){} };
struct Tlerp { Tlerp(int a){} };
struct AgGlar { int const a; };
struct AgTlerp { int const a; };
struct AgBurp { int a; };
struct AgSlerp { int a; };

template <typename ... Args>
struct Inherit : Args...
{
    using Args::Args...;
};

Inherit<Glar, Tlerp>     l;       // error: cannot construct as there are no default constructors.
Inherit<Glar, Tlerp>     m{1, 3}; // error: no matching constructor.
Inherit<AgGlar, AgTlerp> n;       // error: cannot construct as there are no default constructors. No change to current behaviour.
Inherit<AgBurp, AgSlerp> o;       // ok: The data members are uninitialized in this case
Inherit<AgBurp, AgSlerp> p{1, 3}; // error: Attempt to inherit non-default construction from multiple bases.
```
With multiple base classes, code that is currently an error, is still an error.
